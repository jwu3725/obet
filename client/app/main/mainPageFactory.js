'use strict';

angular.module('obetWebAppApp')
  .service('mainPageService', function (){
  	
  	var query = "";

  	this.setQuery = function(item){
  		this.query = item;
  		console.log("set query " + this.query);
  	};

  	this.getQuery = function(){
  		return this.query;
  	};

  });
'use strict';

angular.module('obetapp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .when('/search', {
        templateUrl: 'app/search/search.html',
        controller: 'SearchCtrl'
      })
      .when('/info', {
        templateUrl: 'app/information/info.html',
        controller: 'InfoCtrl'
      });

  });
'use strict';   

var myApp = angular.module('myApp', [
 'ngRoute',
]);

myApp.config(['$routeProvider',
     function($routeProvider) {
         $routeProvider.
             when('/', {
                 templateUrl: '../static/partials/home.html',
             }).
             when('/about', {
                 templateUrl: '../static/partials/about.html',
             }).
             when('/search', {
             	 teplateUrl: '../static/partials/search.html',
             }).
             when('/login', {
                 templateUrl: '../static/partials/login.html',
                 controller: 'LoginController'
             }).
             when('/logout', {
                 templateUrl: '../static/partials/home.html',
                 controller: 'LogoutController'
             }).
             otherwise({
                 redirectTo: '/'
             });
    }]);
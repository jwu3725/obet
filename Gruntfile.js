'use strict';

module.exports = function (grunt) {

  grunt.initConfig({

    // You can also set the value of a key as parsed JSON.
    // Allows us to reference properties we declared in package.json.
    pkg: grunt.file.readJSON('package.json'),

    //require('load-grunt-tasks')(grunt);
    // You can set arbitrary key-value pairs.
    distFolder: 'dist',

    htmlbuild: {
        dist: {
            src: 'index.html',
            dest: 'dist/',
            options: {
                beautify: true,
                prefix: '//some-cdn',
                relative: true,
                scripts: {
                    bundle: [
                        '<%= fixturesPath %>/app/*.js',
                        '!**/main.js',
                    ],
                    main: '<%= fixturesPath %>/main.js'
                }
                /*
                styles: {
                    bundle: [
                        '<%= fixturesPath %>/css/libs.css',
                        '<%= fixturesPath %>/css/dev.css'
                    ],
                    test: '<%= fixturesPath %>/css/inline.css'
                },
                */
               //sections: {
                 //   views: '<%= fixturesPath %>/views/**/*.html',
                   // templates: '<%= fixturesPath %>/templates/**/*.html',
                    //layout: {
                     //   header: '<%= fixturesPath %>/layout/header.html',
                      //  footer: '<%= fixturesPath %>/layout/footer.html'
                   // }
                //},
                
                //data: {
                //    // Data to pass to templates 
                //    version: "0.1.0",
                //    title: "test",
                //}
            }
        }
    },

    // wipe the build directory clean
    //clean: {
    //  build: {
    //    src: ['<%= package.BUILD_DIR %>']
    //  },
    //  scripts: {
    //    src: ['<%= package.BUILD_DIR_JS %>' + '*.js', '!' + '<%= package.BUILD_FILE_JS %>']
    //  }
    //},
       // grunt-express will serve the files from the folders listed in `bases`
    // on specified `port` and `hostname`
    express: {
      all: {
        options: {
          port: 3000,
          hostname: "0.0.0.0",
          bases: ['<%= pkg.build-dir %>'],
          livereload: true
        }
      }
    },
    // grunt-open will open your browser at the project's URL
    open: {
      all: {
        // Gets the port from the connect configuration
        path: 'http://localhost:<%= express.all.options.port %>'
      }
    }
    //serve: {
    //  options: {
    //    port: 9000,
    //     'client.js': {
    //        output: 'client.js'
    //      }
    //  }
    //}
  });

  //grunt.loadNpmTask('grunt-serve');
  grunt.loadNpmTasks('grunt-html-build');
  grunt.loadNpmTasks('grunt-open');
  grunt.loadNpmTasks('grunt-express');

  // Register our own custom task alias.
  //grunt.registerTask('serve', ['serve']);
  grunt.registerTask('server', ['htmlbuild', 'express', 'open']);
}

